package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.*;
import cucumber.api.PendingException;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class TransferenciaExtConsolidadaSteps {

    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;
    private TransferenciaExtConsolidadaPage transferenciaExtConsolidadaPage;


    public TransferenciaExtConsolidadaSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciaExtConsolidadaPage = testContext.getPageObjectManager().getTransferenciaExtConsolidadaPage();

    }


    @Dado("^que se ingresa a la opcion Transferir$")
    public void queSeIngresaALaOpcionTransferir() throws Exception {
        transferenciaExtConsolidadaPage.clickTransferir();
    }

    @Entonces("^se presenta la pantalla de Favoritos$")
    public void sePresentaLaPantallaDeFavoritos() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // throw new PendingException();
    }

    @Y("^se hace click en Al Exterior$")
    public void seHaceClickEnAlExterior() throws Throwable {
        transferenciaExtConsolidadaPage.clickExterior();
    }

    @Y("^se hace click en la cuenta debito$")
    public void seHaceClickEnLaCuentaDebito() throws Throwable {
        transferenciaExtConsolidadaPage.clickCuentaDebito();
    }

    @Y("^selecciona la cuenta$")
    public void seleccionaLaCuenta() throws Throwable {
        transferenciaExtConsolidadaPage.selectCuenta();
    }


    @Y("^se selecciona un beneficiario$")
    public void seSeleccionaUnBeneficiario() throws Throwable {
        transferenciaExtConsolidadaPage.selectBeneficiario();
    }

    @Y("^se da click en Moneda$")
    public void seDaClickEnMoneda() throws Throwable {
        transferenciaExtConsolidadaPage.clickMoneda();
    }

    @Y("^ingresa el monto de \"([^\"]*)\"$")
    public void ingresaElMontoDe(String monto) throws Throwable {
        transferenciaExtConsolidadaPage.ingresarMonto(monto);
    }

    @Y("^se da click en el motivo economico$")
    public void seDaClickEnElMotivoEconomico() throws Throwable {
        transferenciaExtConsolidadaPage.clickOpcionMotivo();
    }

    @Y("^se selecciona un motivo$")
    public void seSeleccionaUnMotivo() throws Throwable {
        transferenciaExtConsolidadaPage.selectMotivo();
    }
}



