package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.*;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class PagoATarjetasBISteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;
    private PagoMisTarjetasPage pagoMisTarjetasPage;
    private PagoATarjetasBIPage pagoATarjetasBIPage;


    public PagoATarjetasBISteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        pagoMisTarjetasPage = testContext.getPageObjectManager().getPagoMisTarjetasPage();
        pagoATarjetasBIPage = testContext.getPageObjectManager().getPagoATarjetasBIPage();


    }

    @Y("^se hace click en A Tarjetas BI$")
    public void seHaceClickEnATarjetasBI() throws Throwable {
       pagoATarjetasBIPage.clickTarjetasBI();
    }

    @Y("^se ingresa el monto \"([^\"]*)\"$")
    public void seIngresaElMonto(String monto) throws Throwable {
        pagoATarjetasBIPage.ingresaMonto(monto);
    }

    @Y("^da click en el boton confirmar$")
    public void daClickEnElBotonConfirmar() throws Throwable {
        transferenciasEntreMisCuentasPage.btnConfirmar();
    }

    @Y("^se selecciona la cuenta$")
    public void seSeleccionaLaCuenta() throws Throwable {
        pagoATarjetasBIPage.selectCuenta();
    }

    @Y("^se ingresa una descripcion \"([^\"]*)\"$")
    public void seIngresaUnaDescripcion(String descripcion) throws Throwable {
        pagoATarjetasBIPage.ingresarDescripcion(descripcion);
    }

    @Y("^se ingresa un monto \"([^\"]*)\"$")
    public void seIngresaUnMonto(String monto) throws Throwable {
        pagoATarjetasBIPage.ingresarMonto(monto);
    }

    @Y("^se hace click en cuenta debito BI$")
    public void seHaceClickEnCuentaDebitoBI() throws Throwable {
        pagoATarjetasBIPage.clickCuentaDebito();
    }
}