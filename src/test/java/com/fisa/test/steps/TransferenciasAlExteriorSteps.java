package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.ConsolidadaPage;
import com.fisa.cucumber.page.TransferenciasAlExteriorPage;
import com.fisa.cucumber.page.TransferenciasEntreMisCuentasPage;
import com.fisa.cucumber.page.TransferenciasOtrasCuentasPage;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class TransferenciasAlExteriorSteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;


    public TransferenciasAlExteriorSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();

    }


    @Y("^se hace click en la pestana Al Exterior$")
    public void seHaceClickEnLaPestanaAlExterior() throws Throwable {
        transferenciasAlExteriorPage.clickAlExterior();
    }

    @Entonces("^se presenta la pantalla Transferencias Al Exterior$")
    public void sePresentaLaPantallaTransferenciasAlExterior() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }


    @Entonces("^se autocompletan los campos de la sección Información del Beneficiario$")
    public void seAutocompletanLosCamposDeLaSecciónInformaciónDelBeneficiario() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Y("^se da click en la opción Moneda$")
    public void seDaClickEnLaOpciónMoneda() throws Throwable {
        transferenciasAlExteriorPage.clickOpcionMoneda();
    }

    @Y("^se selecciona la moneda \"([^\"]*)\"$")
    public void seSeleccionaLaMoneda(String arg0) throws Throwable {
        transferenciasAlExteriorPage.selectMoneda();
    }

    @Y("^se hace seleccion de la cuenta$")
    public void seHaceSeleccionDeLaCuenta() throws Throwable {
        transferenciasAlExteriorPage.selectCuenta();
    }

    @Y("^ingresa un monto de \"([^\"]*)\"$")
    public void ingresaUnMontoDe(String monto) throws Throwable {
        transferenciasAlExteriorPage.ingresarMonto(monto);
    }

    @Y("^se da click en motivo economico$")
    public void seDaClickEnMotivoEconomico() throws Throwable {
        transferenciasAlExteriorPage.clickOpcionMotivo();
    }

    @Y("^se da clic en el botón omitir$")
    public void seDaClicEnElBotónOmitir() throws Throwable {
        transferenciasAlExteriorPage.btnOmitir();
    }

    @Y("^se selecciona el motivo$")
    public void seSeleccionaElMotivo() throws Throwable {
        transferenciasAlExteriorPage.selectMotivo();
    }

    @Y("^se da clic en el botón confirmar$")
    public void seDaClicEnElBotónConfirmar() throws Throwable {
        transferenciasAlExteriorPage.btnConfirmar();
    }

    @Y("^se selecciona la pregunta$")
    public void seSeleccionaLaPregunta() throws Throwable {
        transferenciasAlExteriorPage.SelectPregunta();
    }

    @Y("^se escoge la pregunta$")
    public void seEscogeLaPregunta() throws Throwable {
        transferenciasAlExteriorPage.EscogerPregunta();
    }

    @Cuando("^se ingresa la respuesta a la pregunta \"([^\"]*)\"$")
    public void se_ingresa_la_respuesta_a_la_pregunta(String respuesta) throws Exception {
        transferenciasAlExteriorPage.ingresarRespuesta(respuesta);
    }

    @Cuando("^se da click en el boton confirmar$")
    public void se_da_click_en_el_boton_confirmar() throws Exception {
        transferenciasAlExteriorPage.Confirmar();
    }

    @Y("^se da clic en el beneficiaro$")
    public void seDaClicEnElBeneficiaro() throws Throwable {
        transferenciasAlExteriorPage.clickOpcionBeneficiario();
    }

    @Y("^se selecciona el beneficiaro$")
    public void seSeleccionaElBeneficiaro() throws Throwable {
        transferenciasAlExteriorPage.selectBeneficiario();
    }

    @Y("^se ingresa la referencia \"([^\"]*)\"$")
    public void seIngresaLaReferencia(String referencia) throws Throwable {
        transferenciasAlExteriorPage.ingresarReferencia(referencia);
    }


}

