package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.*;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class PagoServiciosSteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;
    private PagoMisTarjetasPage pagoMisTarjetasPage;
    private PagoATarjetasBIPage pagoATarjetasBIPage;
    private PagoServiciosPage pagoServiciosPage;


    public PagoServiciosSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        pagoMisTarjetasPage = testContext.getPageObjectManager().getPagoMisTarjetasPage();
        pagoATarjetasBIPage = testContext.getPageObjectManager().getPagoATarjetasBIPage();
        pagoServiciosPage = testContext.getPageObjectManager().getPagoServiciosPage();


    }

    @Cuando("^se hace clic en el opcion Pago de Servicios$")
    public void seHaceClicEnElOpcionPagoDeServicios() throws Throwable {
        pagoServiciosPage.clickPagoServicios();
    }

    @Entonces("^se presenta la pantalla de Ingreso de Datos Pago de Servicios$")
    public void sePresentaLaPantallaDeIngresoDeDatosPagoDeServicios() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // throw new PendingException();
    }

    @Y("^se da click en empresa servicio$")
    public void seDaClickEnEmpresaServicio() throws Throwable {
        pagoServiciosPage.clickEmpresaServicios();
    }

    @Y("^se introduce el concepto \"([^\"]*)\"$")
    public void seIntroduceElConcepto(String concepto) throws Throwable {
        pagoServiciosPage.ingresarConcepto(concepto);
    }


    @Y("^se hace click en el boton buscar$")
    public void seHaceClickEnElBotonBuscar() throws Throwable {
        pagoServiciosPage.btnBuscar();
    }

    @Y("^se selecciona la empresa$")
    public void seSeleccionaLaEmpresa() throws Throwable {
        pagoServiciosPage.selectEmpresa();
    }

    @Y("^da click en continuar$")
    public void daClickEnContinuar() throws Throwable {
        pagoServiciosPage.btnContinuar();
    }

   /* @Y("^se selecciona beneficiario$")
    public void seSeleccionaBeneficiario() throws Throwable {
        pagoServiciosPage.selectBeneficiario();
    }

    @Y("^se da click en seleccionar$")
    public void seDaClickEnSeleccionar() throws Throwable {
        pagoServiciosPage.btnSeleccionar();
    }*/

    @Y("^se hace click en Tipo de Pago$")
    public void seHaceClickEnTipoDePago() throws Throwable {
        pagoServiciosPage.clickTipoPago();
    }

    @Y("^se hace select en Debito cuenta$")
    public void seHaceSelectEnDebitoCuenta() throws Throwable {
       pagoServiciosPage.selectCuentaDebito();
    }


    @Y("^ingresa monto igual a \"([^\"]*)\"$")
    public void ingresaMontoIgualA(String monto) throws Throwable {
        pagoServiciosPage.ingresoMonto(monto);
    }

    @Y("^da click en boton continuar$")
    public void daClickEnBotonContinuar() throws Throwable {
        pagoServiciosPage.clickContinuar();
    }
}