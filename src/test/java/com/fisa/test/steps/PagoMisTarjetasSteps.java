package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.*;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class PagoMisTarjetasSteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;
    private PagoMisTarjetasPage pagoMisTarjetasPage;


    public PagoMisTarjetasSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        transferenciasAlExteriorPage = testContext.getPageObjectManager().getTransferenciasAlExteriorPage();
        pagoMisTarjetasPage = testContext.getPageObjectManager().getPagoMisTarjetasPage();


    }


    @Cuando("^se hace clic en el opcion Pago de Tarjeta de Credito$")
    public void seHaceClicEnElOpcionPagoDeTarjetaDeCredito() throws Throwable {
        pagoMisTarjetasPage.clickTarjetas();
    }

    @Entonces("^se presenta la pantalla de Ingreso de Datos Pago de Mis Tarjetas$")
    public void sePresentaLaPantallaDeIngresoDeDatosPagoDeMisTarjetas() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }


    @Y("^se da click en Seleccionar Tarjeta$")
    public void seDaClickEnSeleccionarTarjeta() throws Throwable {
       pagoMisTarjetasPage.clickSeleccionarTarjetas();
    }

    @Y("^se selecciona la tarjeta \"([^\"]*)\"$")
    public void seSeleccionaLaTarjeta(String arg0) throws Throwable {
        pagoMisTarjetasPage.selectTarjeta();
    }

    @Y("^se introduce el monto \"([^\"]*)\"$")
    public void seIntroduceElMonto(String monto) throws Throwable {
        pagoMisTarjetasPage.introduceMonto(monto);
    }

    @Y("^se hace click en Mis Tarjetas BI$")
    public void seHaceClickEnMisTarjetasBI() throws Throwable {
        pagoMisTarjetasPage.clickMisTarjetas();
    }

    @Y("^se selecciona una cuenta$")
    public void seSeleccionaUnaCuenta() throws Throwable {
        pagoMisTarjetasPage.selectCuenta();
    }

    @Y("^se hace click cuenta debito$")
    public void seHaceClickCuentaDebito() throws Throwable {
        pagoMisTarjetasPage.clickCuentaDebito();
    }
}