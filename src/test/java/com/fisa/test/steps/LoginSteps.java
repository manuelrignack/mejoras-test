package com.fisa.test.steps;
import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.ConsolidadaPage;
import com.fisa.cucumber.page.LoginPage;
import com.fisa.cucumber.page.HomePage;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

import static org.junit.Assert.assertEquals;

public class LoginSteps {
    private TestContext testContext;
    private HomePage homePage;
    private LoginPage loginPage;
    private ConsolidadaPage consolidadaPage;

    public LoginSteps(TestContext testContext) {
        this.testContext = testContext;
        homePage = testContext.getPageObjectManager().getHomePage();
        loginPage = testContext.getPageObjectManager().getLoginPage();
        consolidadaPage = testContext.getPageObjectManager().getConsolidadaPage();

    }

    @Dado("^Usuario ingresa a la pagina \"([^\"]*)\"$")
    public void usuario_ingresa_a_la_pagina(String url) throws Exception {
       homePage.navegar(url);

    }

    @Cuando("^el usuario ingresa el nombre \"([^\"]*)\"$")
    public void el_usuario_ingresa_el_nombre(String usuario) throws Exception {
       homePage.ingresoUsuario(usuario);
    }

    @Cuando("^ingresa la contraseña \"([^\"]*)\"$")
    public void ingresa_la_contraseña(String pass) throws Exception {
        loginPage.ingresoContrasena(pass);
    }

      @Y("^se da click en continuar$")
    public void seDaClickEnContinuar() throws Throwable {
        loginPage.btnContinuar();
    }

    @Y("^da clic en el boton continuar$")
    public void daClicEnElBotonContinuar() throws Throwable {
        Thread.sleep(5000);
        loginPage.clickContinuar ();
        Thread.sleep(10000);

    }

    @Entonces("^se accede a la posicion consolidada$")
    public void seAccedeALaPosicionConsolidada() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();

    }


    @Y("^se da click en pantalla para capturar evidencia$")
    public void seDaClickEnPantallaParaCapturarEvidencia() throws Throwable {
        loginPage.clickPantalla();
    }
}
