package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.ConsolidadaPage;
import com.fisa.cucumber.page.TransferenciasEntreMisCuentasPage;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class TransferenciasEntreMisCuentasSteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;



    public TransferenciasEntreMisCuentasSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
    }



    @Dado("^que se ingresa a la opcion de menu Acciones Frecuentes$")
    public void que_se_ingresa_a_la_opcion_de_menu_Acciones_Frecuentes() throws Exception {
        transferenciasEntreMisCuentasPage.clickAccionesFrecuentes();

    }

    @Y("^la opcion Pagos y Transferencias$")
    public void laOpcionPagosYTransferencias() throws Throwable {
        transferenciasEntreMisCuentasPage.clickPagosYTransferencias();
    }

    @Cuando("^se hace clic en el opcion Transferencias$")
    public void seHaceClicEnElOpcionTransferencias() throws Throwable {
        transferenciasEntreMisCuentasPage.clickTransferencias();
    }

    @Entonces("^se presenta la pantalla de Ingreso de Datos$")
    public void sePresentaLaPantallaDeIngresoDeDatos() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Y("^se hace click en cuenta debito$")
    public void seHaceClickEnCuentaDebito() throws Throwable {
        transferenciasEntreMisCuentasPage.clickCuentaDebito();
    }

    @Y("^se selecciona la cuenta \"([^\"]*)\"$")
    public void seSeleccionaLaCuenta(String arg0) throws Throwable {
       transferenciasEntreMisCuentasPage.selectCuenta();
    }

    @Y("^se hace click en cuenta beneficiaria$")
    public void seHaceClickEnCuentaBeneficiaria() throws Throwable {
        transferenciasEntreMisCuentasPage.clickCuentaBeneficiaria();
    }

    @Y("^se hace select a la cuenta \"([^\"]*)\"$")
    public void seHaceSelectALaCuenta(String arg0) throws Throwable {
        transferenciasEntreMisCuentasPage.selectCuentaB();
    }

    @Y("^se ingresa la descripcion \"([^\"]*)\"$")
    public void seIngresaLaDescripcion(String descripcion) throws Throwable {
        transferenciasEntreMisCuentasPage.ingresaDescripcion(descripcion);
    }

    @Y("^ingresa monto \"([^\"]*)\"$")
    public void ingresaMonto(String monto) throws Throwable {
        transferenciasEntreMisCuentasPage.ingresarMonto(monto);
    }

    @Y("^da click en el boton continuar$")
    public void daClickEnElBotonContinuar() throws Throwable {
        transferenciasEntreMisCuentasPage.btnContinuar();
    }

    @Entonces("^se presenta la pestaña de confirmacion$")
    public void sePresentaLaPestañaDeConfirmacion() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Y("^se hace clic en el boton confirmar$")
    public void seHaceClicEnElBotonConfirmar() throws Throwable {
        transferenciasEntreMisCuentasPage.btnConfirmar();
    }

    @Entonces("^se completa la transaccion$")
    public void seCompletaLaTransaccion() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Y("^se hace clic en Entre Mis Cuentas$")
    public void seHaceClicEnEntreMisCuentas() throws Throwable {
        transferenciasEntreMisCuentasPage.clickEntreMisCuentas();
    }
    }
