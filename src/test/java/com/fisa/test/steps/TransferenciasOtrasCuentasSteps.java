package com.fisa.test.steps;

import com.fisa.cucumber.manager.TestContext;
import com.fisa.cucumber.page.ConsolidadaPage;
import com.fisa.cucumber.page.TransferenciasEntreMisCuentasPage;
import com.fisa.cucumber.page.TransferenciasOtrasCuentasPage;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;

public class TransferenciasOtrasCuentasSteps {
    private TestContext testContext;
    private ConsolidadaPage consolidadPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;


    public TransferenciasOtrasCuentasSteps(TestContext testContext) {
        this.testContext = testContext;
        consolidadPage = testContext.getPageObjectManager().getConsolidadaPage();
        transferenciasEntreMisCuentasPage = testContext.getPageObjectManager().getTransferenciasEntreMisCuentasPage();
        transferenciasOtrasCuentasPage = testContext.getPageObjectManager().getTransferenciasOtrasCuentasPage();

    }


    @Y("^se hace click en Otras Cuentas$")
    public void seHaceClickEnOtrasCuentas() throws Throwable {
        transferenciasOtrasCuentasPage.clickOtrasCuentas();
    }

    @Entonces("^se presenta la pantalla de Ingreso de Datos Otras Cuentas$")
    public void sePresentaLaPantallaDeIngresoDeDatosOtrasCuentas() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Y("^se hace click en Beneficiario$")
    public void seHaceClickEnBeneficiario() throws Throwable {
        transferenciasOtrasCuentasPage.clickBeneficiario();
    }

    @Y("^se selecciona el beneficiario$")
    public void seSeleccionaElBeneficiario() throws Throwable {
        transferenciasOtrasCuentasPage.selectBeneficiario();
    }

    @Y("^se da click en el boton seleccionar$")
    public void seDaClickEnElBotonSeleccionar() throws Throwable {
        transferenciasOtrasCuentasPage.btnSeleccionar();
    }

    @Y("^se ingresa descripcion \"([^\"]*)\"$")
    public void seIngresaDescripcion(String descripcion) throws Throwable {
        transferenciasOtrasCuentasPage.ingresarDescripcion(descripcion);
    }

    @Y("^ingresa el monto \"([^\"]*)\"$")
    public void ingresaElMonto(String monto) throws Throwable {
        transferenciasOtrasCuentasPage.ingresarMonto(monto);
}

    @Y("^se hace clic en el boton finalizar$")
    public void seHaceClicEnElBotonFinalizar() throws Throwable {
        transferenciasOtrasCuentasPage.btnFinalizar();
    }

    @Y("^se selecciona la cuenta OC$")
    public void seSeleccionaLaCuentaOC() throws Throwable {
        transferenciasOtrasCuentasPage.selectCuenta();
    }
}