# language: es

  Característica: Login

    Escenario: Login correcto con validaciones de seguridad
      Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
      Cuando el usuario ingresa el nombre "prbbntfisa03"
      Y se da click en continuar
      Y ingresa la contraseña "FisaProd02"
      Y da clic en el boton continuar
      Y se da clic en el botón confirmar
      Y se selecciona la pregunta
      Y se escoge la pregunta
      Y se ingresa la respuesta a la pregunta "prueba"
      Y se da click en el boton confirmar
      Y se da clic en el botón omitir
      Entonces  se accede a la posicion consolidada
      Y se da click en pantalla para capturar evidencia


