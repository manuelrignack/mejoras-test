# language: es

Característica: Pago de Servicios

  Antecedentes: Login correcto
    Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
    Cuando el usuario ingresa el nombre "prbbntfisa01"
    Y se da click en continuar
    Y ingresa la contraseña "Fisaprod06"
    Y da clic en el boton continuar
    Y se da clic en el botón confirmar
    Y se selecciona la pregunta
    Y se escoge la pregunta
    Y se ingresa la respuesta a la pregunta "pruebas"
    Y se da click en el boton confirmar
    Y se da clic en el botón omitir
    Entonces  se accede a la posicion consolidada



  @PagoServicios
    Escenario: Pago De Servicios
    Dado que se ingresa a la opcion de menu Acciones Frecuentes
    Y la opcion Pagos y Transferencias
    Cuando se hace clic en el opcion Pago de Servicios
    Entonces se presenta la pantalla de Ingreso de Datos Pago de Servicios
    Y se da click en empresa servicio
    Y se introduce el concepto "amagua"
    Y se hace click en el boton buscar
    Y se selecciona la empresa
    Y da click en continuar
    #Y se hace click en Beneficiario
    #Y se selecciona beneficiario
    #Y se da click en seleccionar

    Y se hace click en Tipo de Pago
    Y se hace select en Debito cuenta
    Y ingresa monto igual a "5"
    Y da click en boton continuar




