# language: es

Característica: Pago a tarjetas BI

  Antecedentes: Login correcto
    Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
    Cuando el usuario ingresa el nombre "prbbntfisa01"
    Y se da click en continuar
    Y ingresa la contraseña "Fisaprod06"
    Y da clic en el boton continuar
    Y se da clic en el botón confirmar
    Y se selecciona la pregunta
    Y se escoge la pregunta
    Y se ingresa la respuesta a la pregunta "pruebas"
    Y se da click en el boton confirmar
    Y se da clic en el botón omitir
    Entonces  se accede a la posicion consolidada
    Y se da click en pantalla para capturar evidencia


  @PagoATarjetasBI
    Escenario: Pago A Tarjetas BI
    Dado que se ingresa a la opcion de menu Acciones Frecuentes
    Y la opcion Pagos y Transferencias
    Cuando se hace clic en el opcion Pago de Tarjeta de Credito
    Entonces se presenta la pantalla de Ingreso de Datos Pago de Mis Tarjetas
    Y se hace click en A Tarjetas BI
    Y se hace click en cuenta debito BI
    Y se selecciona la cuenta
    Y se hace click en Beneficiario
    Y se selecciona el beneficiario
    Y se da click en el boton seleccionar
    Y se ingresa una descripcion "Test"
    Y se ingresa un monto "3"
    Y da click en el boton continuar
    Y da click en el boton confirmar
    Y se hace clic en el boton finalizar


