# language: es

Característica: Pago mis tarjetas

  Antecedentes: Login correcto
    Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
    Cuando el usuario ingresa el nombre "prbbntfisa03"
    Y se da click en continuar
    Y ingresa la contraseña "FisaProd02"
    Y da clic en el boton continuar
    Y se da clic en el botón confirmar
    Y se selecciona la pregunta
    Y se escoge la pregunta
    Y se ingresa la respuesta a la pregunta "prueba"
    Y se da click en el boton confirmar
    Y se da clic en el botón omitir
    Entonces  se accede a la posicion consolidada
    Y se da click en pantalla para capturar evidencia


  @PagoMisTarjetas
    Escenario: Pago Mis Tarjetas
    Dado que se ingresa a la opcion de menu Acciones Frecuentes
    Y la opcion Pagos y Transferencias
    Cuando se hace clic en el opcion Pago de Tarjeta de Credito
    Entonces se presenta la pantalla de Ingreso de Datos Pago de Mis Tarjetas
    Y se hace click en Mis Tarjetas BI
    Y se hace click cuenta debito
    Y se selecciona una cuenta
    Y se da click en Seleccionar Tarjeta
    Y se selecciona la tarjeta "VISA 439471XXXXXX0153"
    Y se introduce el monto "3"
    Y da click en el boton continuar
    Y se hace clic en el boton confirmar
    Y se hace clic en el boton finalizar


