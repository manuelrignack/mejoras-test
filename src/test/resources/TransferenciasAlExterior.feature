# language: es

Característica: Transferencias al Exterior

  Antecedentes: Login correcto
    Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
    Cuando el usuario ingresa el nombre "prbbntfisa03"
    Y se da click en continuar
    Y ingresa la contraseña "FisaProd02"
    Y da clic en el boton continuar
    Y se da clic en el botón confirmar
    Y se selecciona la pregunta
    Y se escoge la pregunta
    Y se ingresa la respuesta a la pregunta "prueba"
    Y se da click en el boton confirmar
    Y se da clic en el botón omitir
    Entonces  se accede a la posicion consolidada

  @TranferenciasAlExterior
  Escenario: Transferencias Al Exterior
    Dado que se ingresa a la opcion de menu Acciones Frecuentes
    Y la opcion Pagos y Transferencias
    Cuando se hace clic en el opcion Transferencias
    Entonces se presenta la pantalla de Ingreso de Datos
    Y se hace click en la pestana Al Exterior
    Entonces se presenta la pantalla Transferencias Al Exterior
    Y se hace click en cuenta debito
    Y se hace seleccion de la cuenta
    Y se hace click en Beneficiario
    Y se selecciona el beneficiario
    Y se da click en el boton seleccionar
    Entonces se autocompletan los campos de la sección Información del Beneficiario
    Y se da click en la opción Moneda
    Y se selecciona la moneda "Dolar"
    Y ingresa un monto de "5"
    Y se da click en motivo economico
    Y se selecciona el motivo
    Y se da clic en el beneficiaro
    Y se selecciona el beneficiaro
    Y se ingresa la referencia "Ref"
    Y da click en el boton continuar
    Y se hace clic en el boton confirmar
    Y se hace clic en el boton finalizar





