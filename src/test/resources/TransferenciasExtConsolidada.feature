# language: es

Característica: Transferencias al Exterior Consolidada

  Antecedentes: Login correcto
    Dado Usuario ingresa a la pagina "http://192.168.5.206:9080/mcm-web-baninter/logout.jsp"
    Cuando el usuario ingresa el nombre "prbbntfisa03"
    Y se da click en continuar
    Y ingresa la contraseña "FisaProd02"
    Y da clic en el boton continuar
    Y se da clic en el botón confirmar
    Y se selecciona la pregunta
    Y se escoge la pregunta
    Y se ingresa la respuesta a la pregunta "prueba"
    Y se da click en el boton confirmar
    Y se da clic en el botón omitir
    Entonces  se accede a la posicion consolidada



  @TranferenciasExtConsolidada
  Escenario: Transferencias Al Exterior Consolidada
    Dado que se ingresa a la opcion Transferir
    Entonces se presenta la pantalla de Favoritos
    Y se hace click en Al Exterior
    Entonces se presenta la pantalla Transferencias Al Exterior
    Y se hace click en la cuenta debito
    Y selecciona la cuenta
    Y se hace click en Beneficiario
    Y se selecciona un beneficiario
    Y se da click en el boton seleccionar
    Entonces se autocompletan los campos de la sección Información del Beneficiario
    Y se da click en Moneda
    Y se selecciona la moneda "Dolar"
    Y ingresa el monto de "5"
    Y se da click en el motivo economico
    Y se selecciona un motivo
    Y se da clic en el beneficiaro
    Y se selecciona el beneficiaro
    Y se ingresa la referencia "Ref"
    Y da click en el boton continuar
    Y se hace clic en el boton confirmar
    Y se hace clic en el boton finalizar
