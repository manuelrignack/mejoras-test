package com.fisa.cucumber.manager;

import com.fisa.cucumber.page.*;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {
    private WebDriver driver;
    private HomePage homePage;
    private LoginPage loginPage;
    private ConsolidadaPage consolidadaPage;
    private TransferenciasEntreMisCuentasPage transferenciasEntreMisCuentasPage;
    private TransferenciasOtrasCuentasPage transferenciasOtrasCuentasPage;
    private TransferenciasAlExteriorPage transferenciasAlExteriorPage;
    private PagoMisTarjetasPage pagoMisTarjetasPage;
    private PagoATarjetasBIPage pagoATarjetasBIPage;
    private PagoServiciosPage pagoServiciosPage;
    private TransferenciaExtConsolidadaPage transferenciaExtConsolidadaPage;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

    public LoginPage getLoginPage() {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }

    public ConsolidadaPage getConsolidadaPage() {
        return (consolidadaPage == null) ? consolidadaPage = new ConsolidadaPage(driver) : consolidadaPage;
    }

    public TransferenciasEntreMisCuentasPage getTransferenciasEntreMisCuentasPage() {
        return (transferenciasEntreMisCuentasPage == null) ? transferenciasEntreMisCuentasPage = new TransferenciasEntreMisCuentasPage(driver) : transferenciasEntreMisCuentasPage;
    }

    public TransferenciasOtrasCuentasPage getTransferenciasOtrasCuentasPage() {
        return (transferenciasOtrasCuentasPage == null) ? transferenciasOtrasCuentasPage = new TransferenciasOtrasCuentasPage(driver) : transferenciasOtrasCuentasPage;
    }

    public TransferenciasAlExteriorPage getTransferenciasAlExteriorPage() {
        return (transferenciasAlExteriorPage == null) ? transferenciasAlExteriorPage = new TransferenciasAlExteriorPage(driver) : transferenciasAlExteriorPage;
    }

    public PagoMisTarjetasPage getPagoMisTarjetasPage() {
        return (pagoMisTarjetasPage == null) ? pagoMisTarjetasPage = new PagoMisTarjetasPage(driver) : pagoMisTarjetasPage;
    }

    public PagoATarjetasBIPage getPagoATarjetasBIPage() {
        return (pagoATarjetasBIPage == null) ? pagoATarjetasBIPage = new PagoATarjetasBIPage(driver) : pagoATarjetasBIPage;
    }

    public PagoServiciosPage getPagoServiciosPage() {
        return (pagoServiciosPage == null) ? pagoServiciosPage = new PagoServiciosPage(driver) : pagoServiciosPage;
    }

    public TransferenciaExtConsolidadaPage getTransferenciaExtConsolidadaPage() {
        return (transferenciaExtConsolidadaPage == null) ? transferenciaExtConsolidadaPage = new TransferenciaExtConsolidadaPage(driver) : transferenciaExtConsolidadaPage;
    }


}

