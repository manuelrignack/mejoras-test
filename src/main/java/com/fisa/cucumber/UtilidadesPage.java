package com.fisa.cucumber.page;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

//mail
import java.util.Properties;


public class UtilidadesPage {


    public static String path()throws IOException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        DateFormat hourFormat = new SimpleDateFormat("HH_mm_ss");
        String fecha = dateFormat.format(date);
        String carpeta = "screenshot" + "_" + fecha;
        File miDir = new File(".");
        String raiz = miDir.getCanonicalPath();
        String path = raiz + "/evidencias/" + carpeta;
        return path;
    }



    public void takeScreenshot(WebDriver driver, String nombreImagen) throws IOException {
        UUID idFotos = UUID.randomUUID();
        String path=UtilidadesPage.path();
        File directorio = new File(path);
        directorio.mkdir();
        File imagen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File screenshot = new File(path + "/" + nombreImagen + "_" + idFotos + ".png");
        FileUtils.copyFile(imagen, screenshot);
        //.properties
    }


    public void mail(String escenario)  throws IOException{
        final String username = "username@gmail.com";
        final String password = "password";
        Properties props = new Properties();
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "false");
        props.put("mail.smtp.host", "192.168.0.149");
        props.put("mail.smtp.starttls.enable", "false");
        Session session = Session.getInstance(props);
        ///////////////////////////////////
        String path=UtilidadesPage.path();
        String sDirectorio = path;
        File f = new File(sDirectorio);
        File[] ficheros = f.listFiles();
    /*    for (int x=0;x<ficheros.length;x++){
            // String path2=f.getPath();
            System.out.println(path+"\\"+ficheros[x].getName());
        }*/
        ////////////////////////////////////
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("santiago.vivanco@modinter.com.ec"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("santiago.vivanco@modinter.com.ec"));
            message.setSubject(escenario);
            BodyPart texto = new MimeBodyPart();
            MimeMultipart multiParte = new MimeMultipart();
            texto.setText( "Este correo es informativo, favor no responder a esta direccion de correo, ya que no se encuentra habilitada para recibir mensajes.</p>");
            for (int x=0;x<ficheros.length;x++){
                // String path2=f.getPath();
                BodyPart adjunto = new MimeBodyPart();
                adjunto.setDataHandler(new DataHandler(new FileDataSource(path+"/"+ficheros[x].getName())));
                adjunto.setFileName(ficheros[x].getName()+".png");
                multiParte.addBodyPart(adjunto);
            }
            //message.setContent(htmlText, "text/html");
            multiParte.addBodyPart(texto);
            message.setContent(multiParte);
            Transport.send(message);
            FileUtils.deleteDirectory(new File(String.valueOf(path)));
            System.out.println("Done");
        } catch (MessagingException e) {

            throw new RuntimeException(e);
        }
    }
}
