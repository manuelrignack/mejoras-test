package com.fisa.cucumber.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.fisa.cucumber.page.UtilidadesPage;
import java.io.IOException;


public class PagoATarjetasBIPage extends UtilidadesPage {
    private WebDriver driver;
    private WebDriverWait wait;


    public PagoATarjetasBIPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
    }


    @FindBy(how = How.ID, using = "menu99201_text")
    private WebElement clickTarjetasBI;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_19")
    private WebElement ingrsaMonto;

    @FindBy(how = How.XPATH, using = "//td[text()='9777917']")
    private WebElement selectCuenta;

    @FindBy(how = How.ID, using = "ec_fisa_widget_TextBox_9_textBox")
    private WebElement ingresarDescripcion;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_2")
    private WebElement ingresarMonto;

    @FindBy(how =How.XPATH, using ="//span[text()='Seleccione un ítem']")
    private WebElement clickCuentaDebito;



    public void clickTarjetasBI() throws InterruptedException {
        Thread.sleep(5000);
        clickTarjetasBI.click();
    }

    public void ingresaMonto(String monto) throws InterruptedException {
        Thread.sleep(5000);
        ingrsaMonto.sendKeys(monto);
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

    public void ingresarDescripcion(String descripcion) throws InterruptedException {
        Thread.sleep(5000);
        ingresarDescripcion.sendKeys(descripcion);
    }

    public void ingresarMonto(String monto) throws InterruptedException, IOException {
        Thread.sleep(5000);
        ingresarMonto.sendKeys(monto);
        Thread.sleep(9000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }


    public void clickCuentaDebito() throws InterruptedException {
        Thread.sleep(9000);
        clickCuentaDebito.click();
    }





}
