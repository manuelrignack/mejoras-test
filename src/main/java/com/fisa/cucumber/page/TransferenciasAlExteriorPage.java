package com.fisa.cucumber.page;

        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.How;
        import org.openqa.selenium.support.PageFactory;
        import org.openqa.selenium.support.ui.WebDriverWait;
        import com.fisa.cucumber.page.UtilidadesPage;

        import java.io.IOException;

public class TransferenciasAlExteriorPage extends com.fisa.cucumber.page.UtilidadesPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public TransferenciasAlExteriorPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
    }


    @FindBy(how = How.ID, using = "menu99189_text")
    private WebElement clickAlExterior;

    @FindBy(how = How.ID, using = "dijit_form_Select_13")
    private WebElement clickOpcionMoneda;

    @FindBy(how = How.XPATH, using = "//td[text()='DOLAR']")
    private WebElement selectMoneda;

    @FindBy(how = How.XPATH, using = "//td[text()='390101914']")
    private WebElement selectCuenta;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_8")
    private WebElement ingresarMonto;

    @FindBy(how = How.XPATH, using = "(//input[@value='▼ '])")
    private WebElement selectPregunta;

    @FindBy(how = How.ID, using = "dijit_MenuItem_2_text")
    private WebElement escogerPregunta;

    @FindBy(how = How.ID, using = "dijit_form_TextBox_0")
    private WebElement respuesta;

    @FindBy(how = How.ID, using = "dijit_form_Button_2_label")
    private WebElement confirmar;

    @FindBy(how = How.ID, using = "dijit_form_Select_14")
    private WebElement clickOpcionMotivo;

    @FindBy(how = How.XPATH, using = "//span[text()='Confirmar']")
    private WebElement btnConfirmar;

    @FindBy(how = How.XPATH, using = "//span[text()='Omitir']")
    private WebElement btnOmitir;

   /* @FindBy(how = How.XPATH, using = "//span[text()='205 - VIAJES, SALVO SERVICIOS DE TRANSPORTE DE PASAJEROS ']")
    private WebElement selectMotivo;

    dijit_MenuItem_22*/

    @FindBy(how = How.ID, using = "dijit_MenuItem_21")
    private WebElement selectMotivo;

    @FindBy(how =How.XPATH, using ="//span[text()='Seleccione un ítem']")
    private WebElement clickOpcionBeneficiario;

    @FindBy(how = How.XPATH, using = "//td[text()='N - OUR']")
    private WebElement selectBeneficiario;

    @FindBy(how = How.ID, using = "ec_fisa_widget_TextArea_1_textArea")
    private WebElement ingresarReferencia;




    /*public TransferenciasAlExteriorPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }*/

    public void clickAlExterior() throws InterruptedException {
        Thread.sleep(5000);
        clickAlExterior.click();
        Thread.sleep(5000);
    }

    public void clickOpcionMoneda() throws InterruptedException {
        Thread.sleep(10000);
        clickOpcionMoneda.click();
        Thread.sleep(5000);
    }

    public void selectMoneda() throws InterruptedException {
        Thread.sleep(5000);
        selectMoneda.click();
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

    public void ingresarMonto(String monto) throws InterruptedException {
        Thread.sleep(5000);
        ingresarMonto.sendKeys(monto);
        Thread.sleep(9000);
    }

    public void SelectPregunta() throws InterruptedException {
        Thread.sleep(5000);
        selectPregunta.click();
        Thread.sleep(5000);

    }

    public void EscogerPregunta() throws InterruptedException {
        Thread.sleep(5000);
        escogerPregunta.click();
        Thread.sleep(5000);

    }

    public void ingresarRespuesta(String res) throws InterruptedException, IOException{
        Thread.sleep(5000);
        respuesta.sendKeys(res);
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);

    }


    public void Confirmar() throws InterruptedException, IOException {
        Thread.sleep(5000);
        confirmar.click();
        Thread.sleep(6000);

    }

    public void clickOpcionMotivo() throws InterruptedException {
        Thread.sleep(5000);
        clickOpcionMotivo.click();
        Thread.sleep(10000);
    }

    public void btnOmitir() throws Throwable {
        Thread.sleep(5000);
        btnOmitir.click ();
        Thread.sleep(12000);

    }

    public void selectMotivo() throws InterruptedException {
        Thread.sleep(5000);
        selectMotivo.click();
        Thread.sleep(5000);
    }

    public void btnConfirmar() throws Throwable {
        Thread.sleep(5000);
        btnConfirmar.click ();
        Thread.sleep(9000);

    }

    public void clickOpcionBeneficiario() throws InterruptedException {
        Thread.sleep(9000);
        clickOpcionBeneficiario.click();
    }

    public void selectBeneficiario() throws InterruptedException {
        Thread.sleep(5000);
        selectBeneficiario.click();
        Thread.sleep(5000);
    }

    public void ingresarReferencia(String referencia) throws InterruptedException {
        Thread.sleep(5000);
        ingresarReferencia.sendKeys(referencia);
        Thread.sleep(9000);
    }

}
