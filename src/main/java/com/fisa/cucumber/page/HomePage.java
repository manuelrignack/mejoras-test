package com.fisa.cucumber.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    public WebDriver driver;

    @FindBy(how = How.ID, using = "j_username")
    private WebElement nombreUsuario;

    @FindBy(how = How.ID, using = "dijit_form_Button_0_label")
    private WebElement btnContinuar;


    public HomePage(WebDriver driver)  {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void navegar(String url){

        driver.get(url);
    }

    public void ingresoUsuario(String usuario){
        nombreUsuario.clear();
        nombreUsuario.sendKeys(usuario);
    }

    public void clickContinuar(){
        btnContinuar.click();
    }





}
