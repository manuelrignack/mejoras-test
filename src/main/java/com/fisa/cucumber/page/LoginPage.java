package com.fisa.cucumber.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.fisa.cucumber.page.UtilidadesPage;
import java.io.IOException;



    public class LoginPage extends UtilidadesPage{
    private WebDriver driver;
    private WebDriverWait wait;


    public LoginPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
            this.driver = driver;
            wait = new WebDriverWait(driver, 10000);
        }



    @FindBy(how = How.ID, using = "j_password")
    private WebElement contrasena;

    @FindBy(how = How.ID, using = "j_username")
    private WebElement nombreUsuario;

    @FindBy(how = How.ID, using = "dijit_form_Button_0_label")
    private WebElement btnContinuar;

    @FindBy(how = How.ID, using = "dijit_form_Button_2_label")
    private WebElement clickContinuar;

    @FindBy(how = How.ID, using = "dijit_TitlePane_0_titleBarNode")
    private WebElement clickPantalla;




        public void ingresoContrasena(String password) throws InterruptedException,IOException {
        Thread.sleep(3000);
        contrasena.clear();
        contrasena.sendKeys(password);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);

    }

      public String verficarUsuario() throws InterruptedException {
        Thread.sleep(2000);
        String usuario=nombreUsuario.getAttribute("value");
        return usuario;
    }

    public void btnContinuar() throws InterruptedException {
        Thread.sleep(6000);
        btnContinuar.click();
    }

    public void clickContinuar() throws InterruptedException {
        Thread.sleep(6000);
        clickContinuar.click();
    }

        public void clickPantalla() throws InterruptedException, IOException {
            Thread.sleep(6000);
            clickPantalla.click();
            String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
            takeScreenshot(driver,nombreImagen);
        }


}
