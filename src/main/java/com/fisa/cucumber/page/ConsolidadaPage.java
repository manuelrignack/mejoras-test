package com.fisa.cucumber.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ConsolidadaPage {
    private WebDriver driver;


    @FindBy(how = How.XPATH, using = "//div[@class='userName']/span")
    private WebElement nombreUsuario;

    /*@FindBy(how = How.XPATH, using = "//span[text()='Acciones Frecuentes']")
    private WebElement accionesFrecuentes;

    @FindBy(how = How.XPATH, using = "//span[text()='Pagos y Transferencias']")
    private WebElement pagosTransferencias;*/

    public ConsolidadaPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }

    public String validarUsuario() throws InterruptedException {
        Thread.sleep(9000);
        String user= nombreUsuario.getText();
        return user;
    }


}
