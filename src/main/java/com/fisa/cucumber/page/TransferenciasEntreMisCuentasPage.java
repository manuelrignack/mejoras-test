package com.fisa.cucumber.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.fisa.cucumber.page.UtilidadesPage;
import java.io.IOException;



    public class TransferenciasEntreMisCuentasPage  extends UtilidadesPage{
    private WebDriver driver;
    private WebDriverWait wait;

    public TransferenciasEntreMisCuentasPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
            this.driver = driver;
            wait = new WebDriverWait(driver, 10000);
        }



    @FindBy(how = How.XPATH, using = "//span[text()='Acciones Frecuentes']")
    private WebElement clickAccionesFrecuentes;

    @FindBy(how = How.XPATH, using = "//span[text()='Pagos y Transferencias']")
    private WebElement clickPagosYTransferencias;

    @FindBy(how = How.ID, using = "menu99185_text")
    private WebElement clickTransferencias;

    @FindBy(how = How.ID, using = "menu99187_text")
    private WebElement clickEntreMisCuentas;

    @FindBy(how =How.XPATH, using ="//span[text()='Seleccione un ítem']")
    private WebElement clickCuentaDebito;

    @FindBy(how = How.XPATH, using = "//td[text()='390101914']")
    private WebElement selectCuenta;

    @FindBy(how = How.ID, using = "dijit_form_Select_7")
    private WebElement clickCuentaBeneficiaria;

    @FindBy(how = How.ID, using = "dijit_MenuItem_11")
    private WebElement selectCuentaB;

    /*dijit_MenuItem_20_text*/
    /*dijit_MenuItem_10_text*/

    /*@FindBy(how = How.XPATH, using = "//td[text()='232414']")
    private WebElement selectCuentaB;*/


    @FindBy(how = How.ID, using = "ec_fisa_widget_TextBox_17_textBox")
    private WebElement ingresaDescripcion;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_8")
    private WebElement ingresarMonto;

    @FindBy(how = How.XPATH, using = "//span[text()='Continuar']")
    private WebElement btnContinuar;

    @FindBy(how = How.XPATH, using = "//span[text()='Confirmar']")
    private WebElement btnConfirmar;

    @FindBy(how = How.XPATH, using = "//span[text()='Finalizar']")
    private WebElement btnFinalizar;


    public void clickAccionesFrecuentes() throws InterruptedException {
        Thread.sleep(12000);
        clickAccionesFrecuentes.click();
    }

    public void clickPagosYTransferencias() throws InterruptedException {
        Thread.sleep(5000);
        clickPagosYTransferencias.click();
    }

    public void clickEntreMisCuentas() throws InterruptedException {
        Thread.sleep(5000);
        clickEntreMisCuentas.click();
    }

    public void clickTransferencias() throws InterruptedException {
        Thread.sleep(5000);
        clickTransferencias.click();
        Thread.sleep(5000);
    }

    public void clickCuentaDebito() throws InterruptedException {
        Thread.sleep(9000);
        clickCuentaDebito.click();
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

    public void clickCuentaBeneficiaria() throws InterruptedException {
        Thread.sleep(5000);
        clickCuentaBeneficiaria.click();
        Thread.sleep(16000);
    }
    public void selectCuentaB() throws InterruptedException, IOException {
        Thread.sleep(10000);
        selectCuentaB.click();
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void ingresaDescripcion(String descripcion) throws InterruptedException {
        Thread.sleep(3000);
        ingresaDescripcion.sendKeys(descripcion);
    }

    public void ingresarMonto(String monto) throws InterruptedException, IOException {
        Thread.sleep(3000);
        ingresarMonto.sendKeys(monto);
        Thread.sleep(3000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void btnContinuar() throws InterruptedException, IOException {
        Thread.sleep(5000);
        btnContinuar.click();
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void btnConfirmar() throws InterruptedException, IOException {
        Thread.sleep(5000);
        btnConfirmar.click();
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }


}
