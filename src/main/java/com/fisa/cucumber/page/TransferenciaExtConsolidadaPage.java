package com.fisa.cucumber.page;

     import org.openqa.selenium.WebDriver;
     import org.openqa.selenium.WebElement;
     import org.openqa.selenium.support.FindBy;
     import org.openqa.selenium.support.How;
     import org.openqa.selenium.support.PageFactory;



public class TransferenciaExtConsolidadaPage {
private WebDriver driver;


    @FindBy(how = How.XPATH, using = "//span[text()='Transferir']")
    private WebElement clickTransferir;

    @FindBy(how = How.ID, using = "menu99189_text")
    private WebElement clickExterior;

    @FindBy(how = How.XPATH, using = "(//input[@value='▼ '])")
    private WebElement clickCuentaDebito;

    @FindBy(how = How.XPATH, using = "//td[text()='9777917']")
    private WebElement selectCuenta;

    @FindBy(how = How.ID, using = "ec_fisa_grid_FisaQtGrid_8_rowSelector_3")
    private WebElement selectBeneficiario;

    @FindBy(how = How.ID, using = "dijit_form_Select_31")
    private WebElement clickMoneda;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_26")
    private WebElement ingresarMonto;

    @FindBy(how = How.ID, using = "dijit_form_Select_32")
    private WebElement clickOpcionMotivo;

    @FindBy(how = How.ID, using = "dijit_MenuItem_36")
    private WebElement selectMotivo;




    public TransferenciaExtConsolidadaPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }



    public void clickTransferir() throws InterruptedException {
        Thread.sleep(12000);
        clickTransferir.click();
    }

    public void clickExterior() throws InterruptedException {
        Thread.sleep(5000);
        clickExterior.click();
        Thread.sleep(5000);
    }

    public void clickCuentaDebito() throws InterruptedException {
        Thread.sleep(9000);
        clickCuentaDebito.click();
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

    public void selectBeneficiario() throws InterruptedException {
        Thread.sleep(6000);
        selectBeneficiario.click();
        Thread.sleep(5000);
    }

    public void clickMoneda() throws InterruptedException {
        Thread.sleep(10000);
        clickMoneda.click();
        Thread.sleep(5000);
    }

    public void ingresarMonto(String monto) throws InterruptedException {
        Thread.sleep(5000);
        ingresarMonto.sendKeys(monto);
        Thread.sleep(9000);
    }

    public void clickOpcionMotivo() throws InterruptedException {
        Thread.sleep(5000);
        clickOpcionMotivo.click();
        Thread.sleep(10000);
    }

    public void selectMotivo() throws InterruptedException {
        Thread.sleep(5000);
        selectMotivo.click();
        Thread.sleep(5000);
    }




}



