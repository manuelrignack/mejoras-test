package com.fisa.cucumber.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;

public class TransferenciasOtrasCuentasPage extends com.fisa.cucumber.page.UtilidadesPage {
    private WebDriver driver;
    private WebDriverWait wait;

public TransferenciasOtrasCuentasPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
            this.driver = driver;
            wait = new WebDriverWait(driver, 10000);
        }



    @FindBy(how = How.ID, using = "menu99258_text")
    private WebElement clickOtrasCuentas;

    @FindBy(how = How.ID, using = "ec_fisa_lov_ListOfValues_0_button")
    private WebElement clickBeneficiario;

    @FindBy(how = How.ID, using = "ec_fisa_grid_FisaQtGrid_8_rowSelector_0")
    private WebElement selectBeneficiario;

       @FindBy(how =How.XPATH, using ="//span[text()='Seleccionar']")
    private WebElement btnSeleccionar;

    @FindBy(how = How.ID, using = "ec_fisa_widget_TextBox_21_textBox")
    private WebElement ingresarDescripcion;

    /*ec_fisa_widget_TextBox_43_textBox*/

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_8")
    private WebElement ingresarMonto;
    /*dijit_form_NumberTextBox_10*/

    @FindBy(how = How.XPATH, using = "//span[text()='Finalizar']")
    private WebElement btnFinalizar;

    @FindBy(how = How.XPATH, using = "//td[text()='390101914']")
    private WebElement selectCuenta;





    public void clickOtrasCuentas() throws InterruptedException {
        Thread.sleep(5000);
        clickOtrasCuentas.click();
        Thread.sleep(5000);
    }

    public void clickBeneficiario() throws InterruptedException {
        Thread.sleep(5000);
        clickBeneficiario.click();
        Thread.sleep(6000);
    }

    public void selectBeneficiario() throws InterruptedException {
        Thread.sleep(6000);
        selectBeneficiario.click();
        Thread.sleep(5000);
    }

    public void btnSeleccionar() throws InterruptedException,IOException {
        Thread.sleep(5000);
        btnSeleccionar.click();
        Thread.sleep(13000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void ingresarDescripcion(String descripcion) throws InterruptedException {
        Thread.sleep(6000);
        ingresarDescripcion.sendKeys(descripcion);
        Thread.sleep(12000);
    }

    public void ingresarMonto(String monto) throws InterruptedException, IOException {
        Thread.sleep(5000);
        ingresarMonto.sendKeys(monto);
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void btnFinalizar() throws InterruptedException, IOException {
        Thread.sleep(5000);
        btnFinalizar.click();
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

}
