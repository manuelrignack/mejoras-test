package com.fisa.cucumber.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PagoServiciosPage {
    private WebDriver driver;



    @FindBy(how =How.XPATH, using ="//td[text()='Pago de Servicios']")
    private WebElement clickPagoServicios;

    @FindBy(how =How.ID, using ="ec_fisa_widget_TextBox_0_textBox")
    private WebElement clickEmpresaServicios;

    @FindBy(how = How.ID, using = "ec_fisa_widget_TextBox_0_textBox")
    private WebElement ingresarConcepto;

    @FindBy(how =How.XPATH, using ="//span[text()='Buscar']")
    private WebElement btnBuscar;

    @FindBy(how = How.ID, using = "ec_fisa_grid_FisaQtGrid_0_rowSelector_0")
    private WebElement selectEmpresa;

    @FindBy(how =How.ID, using ="dijit_form_Button_1_label")
    private WebElement btnContinuar;

    //@FindBy(how = How.ID, using = "ec_fisa_grid_FisaQtGrid_1_rowSelector_0")
   // private WebElement selectBeneficiario;

   // @FindBy(how =How.ID, using ="ec_fisa_grid_FisaQtGrid_1_selectButton_label")
   // private WebElement btnSeleccionar;

    @FindBy(how = How.ID, using = "dijit_form_Select_1")
    private WebElement clickTipoPago;

    @FindBy(how = How.ID, using = "dijit_MenuItem_5_text")
    private WebElement selectCuentaDebito;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_0")
    private WebElement ingresoMonto;

    @FindBy(how = How.ID, using = "dijit_form_Button_8_label")
    private WebElement clickContinuar;



    public PagoServiciosPage(WebDriver driver) {
        PageFactory.initElements(driver, this);

    }

    public void clickPagoServicios() throws InterruptedException {
        Thread.sleep(5000);
        clickPagoServicios.click();
    }

    public void clickEmpresaServicios() throws InterruptedException {
        Thread.sleep(5000);
        clickEmpresaServicios.click();
    }

    public void ingresarConcepto(String concepto) throws InterruptedException {
        Thread.sleep(3000);
        ingresarConcepto.sendKeys(concepto);
    }

    public void btnBuscar() throws InterruptedException {
        Thread.sleep(5000);
        btnBuscar.click();
    }

    public void selectEmpresa() throws InterruptedException {
        Thread.sleep(5000);
        selectEmpresa.click();
    }

    public void btnContinuar() throws InterruptedException {
        Thread.sleep(5000);
        btnContinuar.click();
    }

   /* public void selectBeneficiario() throws InterruptedException {
        Thread.sleep(5000);
        selectBeneficiario.click();
    }

    public void btnSeleccionar() throws InterruptedException {
        Thread.sleep(5000);
        btnSeleccionar.click();
    }*/

    public void clickTipoPago() throws InterruptedException {
        Thread.sleep(5000);
        clickTipoPago.click();
    }
    public void selectCuentaDebito() throws InterruptedException {
        Thread.sleep(5000);
        selectCuentaDebito.click();
    }

    public void ingresoMonto(String monto) throws InterruptedException {
        Thread.sleep(5000);
        ingresoMonto.sendKeys(monto);
    }

    public void clickContinuar() throws InterruptedException {
        Thread.sleep(5000);
        clickContinuar.click();
    }



}
