package com.fisa.cucumber.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.fisa.cucumber.page.UtilidadesPage;
import java.io.IOException;



    public class PagoMisTarjetasPage extends  UtilidadesPage {
        private WebDriver driver;
        private WebDriverWait wait;


    public PagoMisTarjetasPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
    }



    @FindBy(how = How.ID, using = "menu99192_text")
    private WebElement clickTarjetas;

    @FindBy(how =How.XPATH, using ="//span[text()='Seleccione un ítem']")
    private WebElement clickSeleccionarTarjetas;

    @FindBy(how = How.XPATH, using = "//td[text()='VISA 439471XXXXXX0153']")
    private WebElement selectTarjeta;

    @FindBy(how = How.ID, using = "dijit_form_NumberTextBox_12")
    private WebElement introduceMonto;

    @FindBy(how = How.ID, using = "menu99200_text")
    private WebElement clickMisTarjetas;

    @FindBy(how = How.XPATH, using = "//td[text()='390101914']")
    private WebElement selectCuenta;

    @FindBy(how =How.XPATH, using ="//span[text()='Seleccione un ítem']")
    private WebElement clickCuentaDebito;



    public void clickTarjetas() throws InterruptedException {
        Thread.sleep(5000);
        clickTarjetas.click();
        Thread.sleep(5000);
    }


    public void clickSeleccionarTarjetas() throws InterruptedException {
        Thread.sleep(6000);
        clickSeleccionarTarjetas.click();
    }

    public void selectTarjeta() throws InterruptedException, IOException {
        Thread.sleep(5000);
       selectTarjeta.click();
        Thread.sleep(5000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void introduceMonto(String monto) throws InterruptedException, IOException {
        Thread.sleep(3000);
        introduceMonto.sendKeys(monto);
        Thread.sleep(3000);
        String nombreImagen =new Exception().getStackTrace()[0].getMethodName();
        takeScreenshot(driver,nombreImagen);
    }

    public void clickMisTarjetas() throws InterruptedException {
        Thread.sleep(5000);
        clickMisTarjetas.click();
    }

    public void selectCuenta() throws InterruptedException {
        Thread.sleep(5000);
        selectCuenta.click();
        Thread.sleep(5000);
    }

    public void clickCuentaDebito() throws InterruptedException {
        Thread.sleep(9000);
        clickCuentaDebito.click();
    }

}
